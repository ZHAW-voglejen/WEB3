import Task from "./Task";

export default class TaskList {

    /**
     * @param {string} title
     * @param {string} [id]
     */
    constructor(title, id) {
        this.title = title
        this.id = id

        /**
         * @type {Array<Task>}
         */
        this.tasks = []

        this.listener = () => {}
    }

    add(task) {
        //this.tasks.push(task)

        this.listener({
            event: 'add',
            taskList: this,
            task: task,
        })
    }

    remove(task) {
        this.tasks = this.tasks.filter(t => t !== task);

        this.listener({
            event: 'remove',
            taskList: this,
            task: task,
        })
    }

    doneChanged(task) {
        this.listener({
            event: 'done',
            taskList: this,
            task: task,
        })
    }

    listen(callback) {
        this.listener = callback;
    }

    getTaskByTitle(title) {
        for (const item of this.tasks) {
            if(item.title == title) {
                return item
            }
        }
        return null
    }

    serialize() {
        const obj = {
            title: this.title,
            tasks: [],
        }

        for (const task of this.tasks) {
            obj.tasks.push(task.serialize())
        }

        return obj;
    }

    static unserialize(obj) {
        const list = new TaskList(obj.title);

        for (const taskObj of obj.tasks) {
            list.tasks.push(Task.unserialize(taskObj))
        }

        return list;
    }
}