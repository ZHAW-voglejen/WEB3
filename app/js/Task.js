const propList = [
    'id',
    'priority',
    'title',
    'date',
    'done',
];

const uuidv4 = require('uuid/v4')

export default class Task {
    constructor(priority, title, date, done, server_id) {
        this.id = server_id
        this.client_id = uuidv4();
        this.priority = priority
        this.title = title
        this.date = date
        this.done = done
    }

    getISODate() {
        const splits = this.date.split('.')
        const date = new Date(splits[2], splits[1], splits[0], 0, 0, 0)

        return date.toISOString()
    }

    static fromISODate(isoDate) {
        const date = new Date(isoDate)

        const year = '' + date.getFullYear()
        let month = '' + date.getMonth()
        let day = '' + date.getDate()

        if (month.length === 1) {
            month = '0' + month
        }

        if (day.length === 1) {
            day = '0' + day
        }

        return day + '.' + month + '.' + year
    }

    isValid() {
        return this.priority && this.title && this.date;
    }

    serialize() {
        const obj = {};

        for (const prop of propList) {
            obj[prop] = this[prop];
        }

        return obj;
    }

    static unserialize(obj) {
        const task = new Task();

        for (const prop of propList) {
            task[prop] = obj[prop];
        }

        return task;
    }
}
