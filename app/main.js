import './css/main'
import {component} from 'riot'
import Root from './components/root.riot'

require('jquery-ui-bundle')
require('jquery-ui-bundle/jquery-ui.min.css')

component(Root)(document.getElementById('root'))
