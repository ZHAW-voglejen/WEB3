import TaskList from "./TaskList";
import Task from "./Task";

const BASE_PATH = 'https://zhaw-issue-tracker-api.herokuapp.com/api'
const LOCAL_STORAGE_KEY = 'project-ids'
const API_KEY = 'voglejen'


const uuidv4 = require('uuid/v4')
export default class ZHAWStorageDriver {

    constructor() {
        this.projectIds = []

        /** @var {Array<TaskList>} */
        this.projects = []
    }

    async init() {
        this._loadProjectIds()
        await this._loadProjects()
    }

    /**
     *
     * @param {TaskList} project
     * @returns {Promise<void>}
     */
    async createProject(project) {
        const payload = {
            client_id: uuidv4(),
            title: project.title,
            active: false,
        }

        const result = await this.callAPI('POST', '/projects', payload)

        if (result.errors) {
            alert('Failure while saving project!')
            console.error('Error', result)
            return
        }

        project.id = result.id

        this.projects.push(project)
    }

    /**
     *
     * @param {TaskList} project
     * @returns {Promise<void>}
     */
    async deleteProject(project) {
        const result = await this.callAPI('DELETE', `/projects/${project.id}`)
        if (result.errors) {
            alert('Failure while deleting project!')
            console.error('Error', result)
            return
        }

        this.projects = this.projects.filter(item => item.id !== project.id)
    }

    async loadProject(id) {
        const result = await this.callAPI('GET', `/projects/${id}`)
        if (result.errors) {
            alert('Failure while getting project!')
            console.error('Error', result)
            return
        }

        const loadedProject = new TaskList(result.title, result.id)

        this.projects.push(loadedProject)
    }

    async updateProject(project) {
        const payload = {
            client_id: 'n/a',
            title: project.title,
            active: false,
        }

        const result = await this.callAPI('PUT', `/projects/${project.id}`, payload)
        if (result.errors) {
            alert('Failure while getting project!')
            console.error('Error', result)
            return
        }
    }

    /**
     *
     * @param {TaskList} project
     * @param {Task} issue
     */
    async createIssue(project, issue) {
        const payload = {
            done: issue.done,
            due_date: issue.getISODate(),
            title: issue.title,
            project_client_id: 'n/a',
            priority: '' + issue.priority,
            client_id: issue.client_id,
        }

        const result = await this.callAPI('POST', `/projects/${project.id}/issues`, payload)

        if (result.errors) {
            alert('Failure while creating issue!')
            console.error('Error', result)
            return
        }

        issue.id = result.id

        project.tasks.push(issue)
    }

    /**
     *
     * @param {TaskList} project
     * @param {Task} issue
     */
    async deleteIssue(project, issue) {
        const result = await this.callAPI('DELETE', `/projects/${project.id}/issues/${issue.id}`)
        if (result.errors) {
            alert('Failure while deleting issue!')
            console.error('Error', result)
            return
        }

        project.tasks = project.tasks.filter(item => item.id !== issue.id)
    }

    /**
     *
     * @param {TaskList} project
     */
    async getIssues(project) {
        const result = await this.callAPI('GET', `/projects/${project.id}/issues`)

        if (result.errors) {
            alert('Failure while getting issues!')
            console.error('Error', result)
            return
        }

        for (const item of result) {
            const issue = new Task(parseInt(item.priority), item.title, Task.fromISODate(item.due_date), item.done, item.id)
            project.tasks.push(issue)
        }
    }

    /**
     *
     * @param {TaskList} project
     * @param {Task} issue
     */
    async updateIssue(project, issue) {
        const payload = {
            done: issue.done,
            due_date: issue.getISODate(),
            title: issue.title,
            project_client_id: 'n/a',
            priority: '' + issue.priority,
            client_id: issue.client_id,
        }

        const result = await this.callAPI('PUT', `/projects/${project.id}/issues/${issue.id}`, payload)

        if (result.errors) {
            alert('Failure while updating issue!')
            console.error('Error', result)
        }
    }

    _loadProjectIds() {
        const localProjects = localStorage.getItem(LOCAL_STORAGE_KEY)
        if (localProjects) {
            this.projectIds = JSON.parse(localProjects)
        }
    }

    _saveProjectIds() {
        this.projectIds = this.projects.map(value => value.id)
        localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(this.projectIds))
    }

    async _loadProjects() {
        const promises = []
        for (const pid of this.projectIds) {
            promises.push(this.loadProject(pid))
        }

        await Promise.all(promises)

        const issuePromises = []
        for (const project of this.projects) {
            issuePromises.push(this.getIssues(project))
        }

        await Promise.all(issuePromises)
    }


    callAPI(method, path, payload) {
        const requestConfig = {
            method: method,
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
        }

        if (payload) {
            requestConfig.body = JSON.stringify(payload)
        }

        return fetch(BASE_PATH + path + `?api_key=${API_KEY}`, requestConfig)
            .then(raw => raw.json())
    }

}