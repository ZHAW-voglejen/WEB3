import TaskList from "./TaskList";

const LOCAL_STORAGE_KEY = 'project-list'

export default class LocalStorageDriver {

    /**
     * @return {Array<TaskList>}
     */
    load() {

        const storage = localStorage.getItem(LOCAL_STORAGE_KEY);

        if (storage == null) {
            return this.defaultTaskList();
        }

        const projects = [];
        for (const item of JSON.parse(storage)) {
            projects.push(TaskList.unserialize(item));
        }
        return projects;
    }

    /**
     *
     * @param {Array<TaskList>} taskLists
     */
    save(taskLists) {

        const data = [];
        for (const project of taskLists) {
            data.push(project.serialize());
        }

        localStorage.setItem('project-list', JSON.stringify(data));
    }

    defaultTaskList() {
        const proj = new TaskList('Test Project')

        return [proj];
    }

}