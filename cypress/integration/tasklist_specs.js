describe('Visit page', function() {
    it('should load the page', function() {
        cy.visit('http://localhost:3000')
        cy.clearLocalStorage()
    })
})

describe('New Project', function() {
    it('should add two new projects and they are ordered', function() {
        cy.get('#f-createproject-name')
            .type('project B')
            .should('have.value', 'project B')
        cy.get('project-create > .form-inline > .btn').click()

        cy.get('#f-createproject-name')
            .type('project A')
            .should('have.value', 'project A')
        cy.get('project-create > .form-inline > .btn').click()

        cy.get('.list-group > :nth-child(1)').contains('project A')
        cy.get('.list-group > :nth-child(2)').contains('project B')
    })
})

describe('New Task', function() {
    it('should add new Task', function() {
        cy.get('#f-newtask-name')
            .type('newTask')
            .should('have.value', 'newTask')

        cy.contains('Add').click()
        cy.get('.task').contains('newTask')
    })
})

describe('Task done', function() {
    it('should toggle the done state of task', function() {
        cy.get('.btn-success').click()

        cy.get('.task').should('have.class', 'task-done')
    })
})

describe('Delete task', function() {
    it('should delete the task', function() {
        cy.get('.btn-danger').click()

        cy.get('.task').should('not.exist')
    })
})

describe('Switch project', function() {
    it('should switch the project Task', function() {
        cy.get('.list-group > :nth-child(2)').click()

        cy.get('.list-group > .active').contains('project B')
    })
})